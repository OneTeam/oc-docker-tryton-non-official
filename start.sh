#!/bin/bash

if [ -s /etc/trytond_populate.conf ]; then
    if [ "$DEVELOP" == "True" ]; then
	trytond -c /etc/trytond.conf --dev --logconf /etc/trytond_logging.conf
    else
	uwsgi --ini /etc/uwsgi.conf
    fi
else
    sleep $SLEEP_TRYTOND_ADMIN &&
    echo  $TRYTONADMINPASS > $TRYTONPASSFILE
    if [ "$Provider" == "Kalenis" ]; then
	trytond-admin -c /etc/trytond.conf -d $POSTGRES_DB --all --email $EMAIL -u user_view -vv
    else
	trytond-admin -c /etc/trytond.conf -d $POSTGRES_DB --all --email $EMAIL -vv
    fi
    echo "1" > /etc/trytond_populate.conf &&
    /bin/bash /opt/start.sh	    
fi


