![Diagram deploy](diagram.png "Deploy")

# Description
Docker tryton-non_official It is a tool for deploying unofficial Tryton modules in Docker, including:
1. Gnuhealth
2. Kalenis
3. NaNtic
4. Datalife
5. Presik
6. Trytonar
7. OneTeam
8. Etrivial

> Gnuhealth and Etrivial most be work whit tryton5.0

| Nombre         | Descripción        |
| -------------- | ------------------ |
| Trytond        | Servidor de Tryton | 
| Trytond-Worker | Ejecución de colas |
| Trytond-Cron   | Tareas programadas |
| Nginx          | uso con Uwsgi      | 

# Unofficial Providers

| Name    | Estiamted Modules  | Versions          | Web Site                  | Repositories
| --------- | -------------- | ------------------ | -------------------------- | ---------------
| Gnuhealth |                | 5.0, 6.0           | https://www.gnuhealth.org/ | 
| Kalenis   | 33             | 6.0                | https://kalenislims.com/   | https://gitea.onecluster.org/Kalenis
| NaNtic    | 1150           | 6.0, 6.4, 6.8      | https://www.nan-tic.com/es | https://github.com/NaN-tic
| Datalife  | 200            | 6.0                | https://datalifeit.es/     | https://gitlab.com/datalifeit/trytond-stock_unit_load
| Presik    | 180            | 6.0                | https://presik.com/        | https://bitbucket.org/presik/
| Trytonar  | 31             | 6.0                | https://www.tryton.org.ar/ | https://github.com/tryton-ar/
| OneTeam   | 26             | 6.0, 6.2, 6.4, 6.6 |                            | https://git.disroot.org/OneTeam


# Docker versions of Tryton and Python

| Tryton | Python | 
| ------ | ------ |
| 5.0    | 3.7    |
| 6.0    | 3.7    |
| 6.2    | 3.7    |
| 6.4    | 3.7    |
| 6.6    | 3.9    |
| 6.8    | 3.9    |


# Dependencies:
Have **docker** 20 or higher installed and **git**

[Multi migrate to gitea](https://git.disroot.org/OneTeam/oc-multi_migrate_to_gitea) will be called at the time of the creation of the docker



# Prerequisites

> If you going to use tryton 6.2, must be use python3.9.
> This change was done by team tryton.

Clone repository: 
``` 
git clone https://git.disroot.org/OneTeam/oc-docker-tryton-non-official.git
cd oc-docker-tryton-non-official
```
Edit Dockerfile and change lines commented example create Docker OneTeam:

```
#version Trytond server example 6.0
TRYTOND_VERSION=6.6
#python version that will run the docker example  3.7 or 3.9 is mandatory in case you use tryton6.2
PYTHON_VERSION=3.9
#if the module repository is private
GITEA_USER=gitea_user
GITEA_PASSWORD=gitea_password
GITEA_ACCESS_TOKEN=gitea_user_token
#Repository Multi Migrate
REPO_MULTI_MIGRATE=OneTeam/oc-multi_migrate_to_gitea.git
#domain server gitea
GITEA_DOMAIN=git.disroot.org
URL_MULTI_MIGRATE=https://${GITEA_USER}:${GITEA_PASSWORD}@${GITEA_DOMAIN}/OneTeam/oc-multi_migrate_to_gitea.git
#By default all organizations are set to false, activate only one. 
#GnuHealth, Etrivial, tryton-ar, OneTeam, OneTeam_Pos, Presik, Datalife, NaNtic, Kalenis
Provider=OneTeam
DIR_MODULES=/usr/local/lib/python${PYTHON_VERSION}/dist-packages/trytond/modules
DB_PASSWORD=SUp3r-pass*DB
POSTGRES_DB=tryton
DB_HOSTNAME=db
POSTGRES_PASSWORD=SUp3r-pass*DB
TRYTON_PORT=8000
DEVELOP=True
WORKER=True
TRYTONPASSFILE=/etc/trytond_admin_pass.conf
#Password user admin
TRYTONADMINPASS=admin
#Email user admin
EMAIL=admin@admin.com
SLEEP_TRYTOND_ADMIN=15
#SMTP 
#This value is True or False
SMTP=False
SMTP_TYPE=smtps+ssl
SMTP_USER=
SMTP_PASSWORD=
SMTP_DOMAIN=
SMTP_PORT=465
SMTP_FROM=NAME_FROM <mail>
SMTP_EMAIL=
```

# Usage

Building a profil.
**Note: in case of than you need rebuild your docker image from the beginning, we will have add the option '--no-cache'**
```
docker-compose -p <profile_name> build --no-cache --progress plain
```

Create a project with the previous profile built
```
docker-compose -p <profile_name> up -d
```

# Bassic Commands.

Enter tryton docker service bash.
**Note: 'tryton' is the name assigned into docker-compose.yml for th docker service of tryton,
'/entrypoint.sh' it is import for load environment variables,
'--user' specifies the user.**

```
docker-compose -p <profile_name> exec --user root tryton /entrypoint.sh bash
```
Update list of modules

```
docker-compose -p <profile_name> exec --user root tryton /entrypoint.sh trytond-admin -c /etc/trytond.conf -d <database> -vv -m --all
```

Enter postgres docker service psql.
```
docker-compose -p <profile_name> exec --user postgres db psql 
```