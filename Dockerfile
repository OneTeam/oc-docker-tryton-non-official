#version Trytond server example 5.0
ARG TRYTOND_VERSION

FROM tryton/tryton:$TRYTOND_VERSION
#lebel custom
LABEL org.label-schema.version="$TRYTON_VERSION"

ARG TRYTOND_VERSION
ARG PYTHON_VERSION
ARG Provider
ARG URL_MULTI_MIGRATE
ARG REPO_MULTI_MIGRATE
ARG GITEA_DOMAIN
ARG GITEA_USER
ARG GITEA_PASSWORD
ARG GITEA_ACCESS_TOKEN
ARG DIR_MODULES
ARG TRYTOND_LOGGING_CONFIG
ARG TRYTOND_LOGGING_LEVEL
ARG DEVELOP
ARG WORKER
ARG EMAIL
ARG TRYTONPASSFILE
ARG URL_MULTI_MIGRATE
ARG SMTP
ARG SMTP_TYPE
ARG SMTP_USER
ARG SMTP_PASSWORD
ARG SMTP_DOMAIN
ARG SMTP_PORT
ARG SMTP_FROM
ARG SMTP_EMAIL

USER root

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       git \
       python3-click \
       python3-ipython

RUN echo "Install modules non official"
COPY trytond.conf /etc/trytond.conf
COPY uwsgi.conf /etc/uwsgi.conf
COPY $TRYTOND_VERSION /opt
COPY start.sh /opt
COPY trytond-modules_non_official.sh /tmp/trytond-modules_non_official.sh
RUN cd /tmp/ \
    && bash trytond-modules_non_official.sh

COPY trytond_logging.conf /etc/trytond_logging.conf
COPY trytond_cron_logging.conf /etc/trytond_cron_logging.conf
COPY trytond_worker_logging.conf /etc/trytond_worker_logging.conf

RUN touch /etc/trytond_populate.conf \
    && touch $TRYTONPASSFILE \
    && chown -R trytond:trytond /etc/trytond* \
    && rm -rf /var/log/trytond \
    && mkdir /var/log/trytond \
    && chown -R trytond:trytond /var/log/trytond \
    && rm -rf /mnt/attachment \
    && mkdir /mnt/attachment \
    && chown -R trytond:trytond /mnt/attachment    
    
USER trytond

CMD ["/bin/bash", "/opt/start.sh"]
