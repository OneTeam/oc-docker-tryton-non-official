appdirs==1.4.4
asn1crypto==1.5.1
attrs==20.3.0
bcrypt==3.1.7
beautifulsoup4==4.9.3
braintree==4.17.1
cached-property==1.5.2
cairocffi==1.2.0
CairoSVG==2.5.0
certifi==2020.6.20
cffi==1.14.5
chardet==4.0.0
click==7.1.2
colorama==0.4.4
contourpy==1.0.6
cryptography==3.3.2
csb43==0.9.2
cssselect2==0.3.0
cycler==0.11.0
defusedxml==0.6.0
elastic-transport==8.4.0
elasticsearch==8.5.0
elementpath==3.0.2
febelfin-coda==0.2.0
fonttools==4.38.0
forex-python==1.8
freezegun==0.3.15
Genshi==0.7.5
gevent==20.9.0
greenlet==0.4.17
html2text==2020.1.16
html5lib==1.1
httplib2==0.18.1
idna==2.10
iso3166==2.1.1
isodate==0.6.0
kiwisolver==1.4.4
ldap3==2.8.1
lxml==4.6.3
matplotlib==3.6.2
mock==4.0.3
numpy==1.23.5
ofxparse==0.19
packaging==21.3
passlib==1.7.4
phonenumbers==8.13.0
Pillow==8.1.2
ply==3.11
polib==1.1.0
proteus==6.4.1
psycopg2==2.8.6
pyactiveresource==2.2.2
pyasn1==0.4.8
pycountry==22.3.5
pycparser==2.20
pycurl==7.43.0.6
pydot==1.4.2
pygal==3.0.0
PyJWT==2.6.0
pyOpenSSL==20.0.1
pyparsing==2.4.7
PyPDF2==1.26.0
Pyphen==0.9.5
PySimpleSOAP==1.16.2
python-dateutil==2.8.1
python-Levenshtein==0.12.2
python-magic==0.4.20
python-sql==1.4.0
python-stdnum==1.18
pytz==2021.1
PyYAML==5.3.1
qrcode==6.1
relatorio==0.10.1
requests==2.25.1
requests-file==1.5.1
requests-toolbelt==0.9.1
schwifty==2022.9.0
scipy==1.6.0
setuptools==52.0.0
ShopifyAPI==11.0.0
simpleeval==0.9.10
simplejson==3.18.0
six==1.16.0
soupsieve==2.2.1
stripe==5.0.0
tinycss2==1.0.2
urllib3==1.26.5
WeasyPrint==51
webencodings==0.5.1
Werkzeug==1.0.1
wheel==0.34.2
wrapt==1.12.1
xades==0.2.2
xcffib==0.8.1
xmlschema==2.1.1
xmlsec==1.3.12
xmlsig==0.1.7
zeep==4.0.0
zope.event==4.4
zope.interface==5.2.0
