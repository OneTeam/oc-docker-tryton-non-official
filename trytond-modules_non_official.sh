#!/usr/bin/bash
gitea_url="https://$GITEA_USER:$GITEA_PASSWORD@${GITEA_DOMAIN}/"

cd /tmp
git clone "${gitea_url}${REPO_MULTI_MIGRATE}"
multi_migrate="/tmp/oc-multi_migrate_to_gitea/migrate_to_gitea.py"
DIR_USR=/usr/local/lib/python
DIR_PACKAGES=/dist-packages/trytond/modules
DIR_MODULES="${DIR_USR}${PYTHON_VERSION}${DIR_PACKAGES}"
cd ${DIR_MODULES}
PYTHON_VERSION="python${PYTHON_VERSION}"

if [ "$WORKER" == "True" ]; then
    cat <<EOF >>/etc/trytond.conf

[queue]
worker = True
EOF
fi


if [ "$SMTP" == "True" ]; then
    cat <<EOF >>/etc/trytond.conf

[email]
uri = ${SMTP_TYPE}://${SMTP_USER}:${SMTP_PASSWORD}@${SMTP_DOMAIN}:${SMTP_PORT}
from = ${SMTP_FROM} <${SMTP_EMAIL}>
EOF
fi


clone_module() {
    cd ${DIR_MODULES}
    local org=$1
    local branch=$2
    local prefix=$3
    shift
    local Array=("${@}")
    url=${gitea_url}${org}"/"
    for module in "${Array[@]}"; do
	if [ ${prefix} = 'False' ]; then
	    if [ ${branch} != "False" ]; then
		if [ -d $module ]; then		
		    echo "Module $module already exists"
		else
		    git clone -b ${branch} $url$module   
		fi
	    fi
	elif [ `echo $module | awk /${prefix}/` ]; then
 	    name=`echo $modules$module | cut --complement -f 1 -d  '-'`
	    if [ ${branch} != "False" ]; then
		if [ -d $module ]; then		
		    echo "Module $module already exists"
		else
		    git clone -b ${branch} $url$module   
		    mv $module $name
		fi
	    else
		git clone $url$module
		cd  $module
		${PYTHON_VERSION} setup.py install
		cd ..
	    fi
	fi
    done
}


inactive_module() {
    cd ${DIR_MODULES}
    local prefix=$1
    local pip=$2
    shift
    local Array=("${@}")
    for module in "${Array[@]}"; do
	if [ $pip = "True" ]; then
	    pip uninstall -y  $prefix$module
	    rm -rf  $module
	else
	    rm -rf $module
	fi
    done    
}


if [ "$Provider" == "OneTeam" ]; then
    apt-get update \
    && apt-get install -y --no-install-recommends \
	git \
        pkg-config \
        libxml2-dev \
        libxmlsec1-dev \
        libxmlsec1-openssl \
        libffi-dev \
        build-essential \
        python3-dev \
        python3-qrcode \
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install --upgrade pip \
    && pip3 install -r /opt/requirements_${Provider}.txt \
    && pip3 install git+https://${GITEA_USER}:${GITEA_PASSWORD}@gitea.onecluster.org/OneTeam/oc-facho.git@documento_soporte \
    && cd ${DIR_MODULES}
	      
    prefix="trytondo-"
    team_org="OneTeam"
    pip="False"        
    get_one_team=( `${PYTHON_VERSION} $multi_migrate list-repo-token --domain ${GITEA_DOMAIN} --team_org $Provider --access_token ${GITEA_ACCESS_TOKEN} --verbose True` )
    clone_module $Provider ${TRYTOND_VERSION} $prefix  "${get_one_team[@]}"

    inactive_modules_trytondo=(
	purchase_coffee
	sale_payment_form
	sale_pos_extras
	optical_equipment
	)
	
    inactive_module "trytond-" $pip "${inactive_modules_trytondo[@]}"
fi


if [ "$Provider" == "OneTeam_Pos" ]; then
    apt-get update \
    && apt-get install -y --no-install-recommends \
	git \
        pkg-config \
        libxml2-dev \
        libxmlsec1-dev \
        libxmlsec1-openssl \
        libffi-dev \
        build-essential \
        python3-dev \
        python3-qrcode \
	python3-ipython \
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install --upgrade pip \
    && pip3 install -r /opt/requirements_OneTeam.txt \
    && pip3 install git+https://${GITEA_USER}:${GITEA_PASSWORD}@gitea.onecluster.org/OneTeam/oc-facho.git@documento_soporte \
    && cd ${DIR_MODULES}
	      
    prefix="trytondo-"
    team_org="OneTeam"
    pip="False"        
    get_one_team=( `${PYTHON_VERSION} $multi_migrate list-repo-token --domain ${GITEA_DOMAIN} --team_org $team_org --access_token ${GITEA_ACCESS_TOKEN} --verbose True` )
    clone_module $team_org ${TRYTOND_VERSION} $prefix  "${get_one_team[@]}"

    inactive_modules_trytondo=(
	purchase_coffee
	sale_payment
	)
	
    inactive_module "trytond-" $pip "${inactive_modules_trytondo[@]}"

    modules_nantic=(
	trytond-sale_w_tax
	trytond-purchase_discount
	trytond-sale_pos
	trytond-sale_shop
	trytond-sale_payment
	trytond-purchase_discount
	trytond-account_invoice_discount )	
    clone_module  "NaNtic" ${TRYTOND_VERSION} "trytond-" "${modules_nantic[@]}"        
fi


if [ "$Provider" == "Presik" ]; then
    prefix="trytonpsk-"
    team_org="Presik"
    pip="False" 
    pip3 install --upgrade pip \
    && pip3 install -r /opt/requirements_${Provider}.txt
    get_presik=( `${PYTHON_VERSION} $multi_migrate list-repo-token --domain ${GITEA_DOMAIN} --team_org ${Provider} --access_token ${GITEA_ACCESS_TOKEN} --verbose True` )
    clone_module $Provider "master" $prefix  "${get_presik[@]}"
    inactive_modules_presik=(
	audiovisual
	document
	document_collection
	document_expiration
	document_communication
	account_aus
	account_invoice_mail
	account_invoice_party_revenue
	electronic_mail_template
	electronic_mail
	staff_document
	staff_document_alert
	sale_pos_frontend
	sale_pos_frontend_rest
	sale_pos_commission
	sale_pos_synchronize
	maintenance-indicators
	health
	health_ambulance
	health_calendar
	health_co
	health_functional
	health_gloss
	health_gyneco
	health_history
	health_icd10
	health_inpatient
	health_lab
	health_lifestyle
	health_nursing
	health_optometry
	health_orthanc
	health_pediatrics
	health_services
	health_socioeconomics
	health_stock
	health_who_essential_medicines
	hotel_zak
	real_estate
	smtp
	sms
	vps
    )
    inactive_module $prefix $pip "${inactive_modules_presik[@]}"

    modules_nantic=(trytond-smtp)
    clone_module  "NaNtic" ${TRYTOND_VERSION} "trytond-" "${modules_nantic[@]}"    
fi


if [ "$Provider" == "Datalife" ]; then
    prefix="trytond-"
    team_org="Datalife"
    pip="False"    
    apt-get install -y --no-install-recommends \
        build-essential \
        python3-dev \
        libffi-dev \
    && pip3 uninstall -y trytond-incoterm \
    && cd /usr/local \
    && git clone -b main https://${GITEA_USER}:${GITEA_PASSWORD}@git.disroot.org/NaNtic/python-edifact.git \
    && cd python-edifact \
    && ${PYTHON_VERSION} setup.py install \
    && cd /usr/local \
    && git clone -b ${TRYTOND_VERSION} https://${GITEA_USER}:${GITEA_PASSWORD}@git.disroot.org/Datalife/tryton_replication.git \
    && cd tryton_replication \
    && ${PYTHON_VERSION} setup.py install \
    && cd /usr/local \
    && git clone -b ${TRYTOND_VERSION} https://${GITEA_USER}:${GITEA_PASSWORD}@git.disroot.org/Datalife/tryton_rpc.git \
    && cd tryton_rpc \
    && ${PYTHON_VERSION} setup.py install \
    && cd $DIR_MODULES  \
    && rm -rf incoterm \
    && pip3 install --upgrade pip \
    && pip3 install -r /opt/requirements_${Provider}.txt

    inactive_modules_trytond=(
	sale_payment)
    inactive_module "trytond-" $pip "${inactive_modules_trytond[@]}"
    
    get_datalife=( `${PYTHON_VERSION} $multi_migrate list-repo-token --domain ${GITEA_DOMAIN} --team_org $Provider --access_token ${GITEA_ACCESS_TOKEN} --verbose True` )
    clone_module $Provider ${TRYTOND_VERSION} $prefix  "${get_datalife[@]}"
    inactive_modules_datalife=(
	edw_mssql
	sale_cost_apply_purchase
	agro_sale_board_harvest_request
	label_printer
	stock_label_printer
	account_payment_confirming_n68
	electronic_mail_scheduler
	electronic_mail_wizard )
    inactive_module $prefix $pip "${inactive_modules_datalife[@]}"

    modules_nantic=(
	trytond-account_payment_type
	trytond-company_bank
	trytond-account_invoice_company_currency
	trytond-jasper_reports
	trytond-stock_scanner
	trytond-stock_valued    
	trytond-stock_kit    
	trytond-sale_kit
	trytond-account_payment_type
	trytond-edocument_unedifact
	trytond-account_move_draft
	trytond-account_payment_receipt
	trytond-product_barcode )
    clone_module  "NaNtic" ${TRYTOND_VERSION} $prefix "${modules_nantic[@]}"
fi


if [ "$Provider" == "NaNtic" ]; then
    prefix="trytond-"
    team_org="NaNtic"
    pip="False"
    apt-get install -y --no-install-recommends \
	build-essential \
	python3-dev \
	libffi-dev \
	libldap2-dev \
	libsasl2-dev \
	libcups2-dev \
	libpq-dev \
    && pip3 install --upgrade pip \
    && cd /opt \
    && git clone --branch ${TRYTOND_VERSION} ${gitea_url}${Provider}/tryton-config.git \
    && pip3 install -r tryton-config/requirements.txt \
    && pip3 install -r /opt/requirements_${Provider}.txt
    get_nantic=( `${PYTHON_VERSION} $multi_migrate list-repo-token --domain ${GITEA_DOMAIN} --team_org $Provider --access_token ${GITEA_ACCESS_TOKEN} --verbose True` )
    clone_module $Provider ${TRYTOND_VERSION} $prefix  "${get_nantic[@]}"
    inactive_modules_nantic=(patches
			     tryton-config
			    )
    inactive_module $prefix $pip "${inactive_modules_nantic[@]}"
fi


if [ "$Provider" == "Kalenis" ]; then
    team_org="kalenis"
    branch="master"
    apt-get install -y --no-install-recommends \
        build-essential \
        python3-dev \
	wget \
    && pip3 install --upgrade pip \
    && pip3 install -r /opt/requirements_${Provider}.txt \
    && git clone -b ${branch} ${gitea_url}${Provider}/kalenislims.git \
    && mv kalenislims/lims* ${DIR_MODULES} \
    && rm -rf kalenislims \
    && git clone -b ${branch} ${gitea_url}${Provider}/kalenis_user_view.git \
    && mv kalenis_user_view ${DIR_MODULES}/user_view \
    && wget https://downloads.kalenislims.com/frontend_dist_${TRYTOND_VERSION}.tar.gz \
    && tar -xzf frontend_dist_${TRYTOND_VERSION}.tar.gz \
    && rm -rf /var/lib/trytond/www \
    && mv frontend_dist_${TRYTOND_VERSION} /var/lib/trytond/www
fi


if [ "$Provider" == "GnuHealth" ]; then
    name_tar="gnuhealth-4.0.4"
    if [ "$TRYTOND_VERSION" == "5.0" ]; then
	name_tar="gnuhealth-3.8.0"
    fi
    apt-get install -y --no-install-recommends \
        build-essential \
        python3-dev \
	wget \
	libldap2-dev \
	libsasl2-dev \
	ldap-utils \
	tox \
	lcov \
	valgrind \
    && pip3 install --upgrade pip \
    && pip3 install -r /opt/requirements_${Provider}.txt \
    && cd /tmp \
    && wget https://ftp.gnu.org/gnu/health/${name_tar}.tar.gz \
    && tar -xzf ${name_tar}.tar.gz \
    && mv ${name_tar}/health* ${DIR_MODULES} \
    && rm -rf ${name_tar}
    if [ "$TRYTOND_VERSION" == "5.0" ]; then
       mv ${DIR_MODULES}/health_webdav3_server ${DIR_MODULES}/webdav
       mv ${DIR_MODULES}/health_caldav ${DIR_MODULES}/calendar    	          
    fi
fi


if [ "$Provider" == "Trytonar" ]; then
    prefix="False"
    team_org="tryton-ar"
    pip="False"
    apt-get install -y --no-install-recommends \
	wget \
	unzip \
	build-essential \
	python3-dev \
	libxslt-dev \
	libffi-dev \
	libssl-dev \
	swig \
	unzip \
    && cd /opt \
    && git clone https://github.com/reingart/pyafipws.git \
    && cd pyafipws \
    && ${PYTHON_VERSION} setup.py install \
    && pip3 install --upgrade pip \
    && pip3 install -r /opt/requirements_${Provider}.txt \
    && cd ${DIR_MODULES}
	
    get_trytonar=( `${PYTHON_VERSION} $multi_migrate list-repo-token --domain ${GITEA_DOMAIN} --team_org $team_org --access_token ${GITEA_ACCESS_TOKEN} --verbose True` )
    clone_module $team_org ${TRYTOND_VERSION} $prefix  "${get_trytonar[@]}"
    inactive_modules_trytonar=(
	documentacion-tryton-ar
	tryton-demo-ar
	tryton-capacitacion-tecnica
	company_ar_NO_USAR
	tryton-ar.github.io
	deploy-localizacion-argentina_DEPRECATE
	TUBA2015
	padron_afip_ar_DEPRECATE
    )
    inactive_module $prefix $pip "${inactive_modules_trytonar[@]}"
fi
