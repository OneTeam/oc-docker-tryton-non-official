![Diagram deploy](diagram.png "Deploy")

# Descripción
Docker tryton-non_official es un despliegue de módulos no oficales de  Tryton, que permite de manera fácil la creación de las siguientes instancias:

| Nombre         | Descripción        |
| -------------- | ------------------ |
| Trytond        | Servidor de Tryton | 
| Trytond-Worker | Ejecución de colas |
| Trytond-Cron   | Tareas programadas |
| Nginx          | uso con Uwsgi      | 

# Proveedores no oficiales

| Nombre    | Módulos apróx  | Versiones          | Sitio Web                  | Repositorios
| --------- | -------------- | ------------------ | -------------------------- | ---------------
| Gnuhealth |                | 5.0, 6.0           | https://www.gnuhealth.org/ | 
| Kalenis   | 33             | 6.0                | https://kalenislims.com/   | https://gitea.onecluster.org/Kalenis
| NaNtic    | 1150           | 6.0, 6.4, 6.8      | https://www.nan-tic.com/es | https://github.com/NaN-tic
| Datalife  | 200            | 6.0                | https://datalifeit.es/     | https://gitlab.com/datalifeit/trytond-stock_unit_load
| Presik    | 180            | 6.0                | https://presik.com/        | https://bitbucket.org/presik/
| Trytonar  | 31             | 6.0                | https://www.tryton.org.ar/ | https://github.com/tryton-ar/
| OneTeam   | 26             | 6.0, 6.2, 6.4, 6.6 |                            | https://git.disroot.org/OneTeam


# Docker versiones de Tryton y Python

| Tryton | Python | 
| ------ | ------ |
| 5.0    | 3.7    |
| 6.0    | 3.7    |
| 6.2    | 3.7    |
| 6.4    | 3.7    |
| 6.6    | 3.9    |
| 6.8    | 3.9    |


# Dependencias:
Tener **docker-compose** 20 ó superior instalado y **git**

[Multi migrate to gitea](https://git.disroot.org/OneTeam/oc-multi_migrate_to_gitea) se llamará en el momento de la creación de los docker.

# Requirimientos.

> Sí usted está usando tryton 6.2, use python3.9.
> Este cambio lo hizo el equipo de Tryton.

Clone este repositorio: 
``` 
git clone https://git.disroot.org/OneTeam/oc-docker-tryton-non-official.git
cd oc-docker-tryton-non-official
```
Edíte el archivo Dockerfile y cambie el valor de las variables,  ejemplo para Crear un Docker de OneTeam:

```
#version del servidor Trytond, ejemplo 6.0
TRYTOND_VERSION=6.6
#version de python que correrá en el docker ejemplo  3.7 ó 3.9 es obligatorio en caso de que uses tryton6.2
PYTHON_VERSION=3.9
#Sí el repositorio del módulo es privado
GITEA_USER=gitea_user
GITEA_PASSWORD=gitea_password
GITEA_ACCESS_TOKEN=gitea_user_token
#Repository Multi Migrate
REPO_MULTI_MIGRATE=OneTeam/oc-multi_migrate_to_gitea.git
#Dominio del Servidor de Gitea
GITEA_DOMAIN=git.disroot.org
URL_MULTI_MIGRATE=https://${GITEA_USER}:${GITEA_PASSWORD}@${GITEA_DOMAIN}/OneTeam/oc-multi_migrate_to_gitea.git
#Por defecto todas las organizaciones están configuradas como falsas, active solo una.
#Nombre de proveedores disponibles
#GnuHealth, Etrivial, tryton-ar, OneTeam, OneTeam_Pos, Presik, Datalife, NaNtic, Kalenis
Provider=OneTeam
DIR_MODULES=/usr/local/lib/python${PYTHON_VERSION}/dist-packages/trytond/modules
DB_PASSWORD=SUp3r-pass*DB
POSTGRES_DB=tryton
DB_HOSTNAME=db
POSTGRES_PASSWORD=SUp3r-pass*DB
TRYTON_PORT=8000
DEVELOP=True
WORKER=True
TRYTONPASSFILE=/etc/trytond_admin_pass.conf
#Contraseña del usuario admin
TRYTONADMINPASS=admin
#Correo del usuario admin
EMAIL=admin@admin.com
SLEEP_TRYTOND_ADMIN=15
#SMTP 
#Este valor es verdadero o falso
SMTP=False
SMTP_TYPE=smtps+ssl
SMTP_USER=
SMTP_PASSWORD=
SMTP_DOMAIN=
SMTP_PORT=465
SMTP_FROM=NAME_FROM <mail>
SMTP_EMAIL=
```

# Uso.

Construyendo un perfíl.
**Nota: En caso de que necesite reconstruir una imagen de docker desde el inicio , agregaremos la opción '--no-cache'**
```
docker-compose -p <nombre_perfil> build --no-cache --progress plain
```

Construir un proyecto utilizando el perfíl construido anteriormente.
```
docker-compose -p <nombre_perfil> up -d
```

# Comandos Básicos.

Ingresar al bash del docker de Tryton.
**Nota: 'tryton' es el nombre del servicio asignado en docker-compose.yml para el docker de tryton,
'/entrypoint.sh' es importante si se desea entrar cargando las variables de entorno,
'--user' especifica el nombre del usuario con el que se desea ingresar.**

```
docker-compose -p <nombre_perfil> exec --user root tryton /entrypoint.sh bash
```

Actualizar listado de Modúlos de una base de datos.
```
docker-compose -p <nombre_perfil> exec --user root tryton /entrypoint.sh trytond-admin -c /etc/trytond.conf -d <database> -vv -m --all
```

Ingresar a la consola de postgres
```
docker-compose -p <nombre_perfil> exec --user postgres db psql 
```

Al inicio se tomo de referencia  https://discuss.tryton.org/t/how-to-run-tryton-using-docker/3200,
puede tomar de refencia https://docs.docker.com/get-started/
